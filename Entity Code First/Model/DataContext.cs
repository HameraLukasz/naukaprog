﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApp3.Model;

namespace Entity_Code_First.Model
{
    public class DataContext :DbContext
    {
        public DataContext() : base(@"Persist Security Info=False;Integrated Security=true;Initial Catalog=ProgNauka;server=(local)")
        {
            
        }
        public DbSet<Person> People { get; set; }
    }
}
