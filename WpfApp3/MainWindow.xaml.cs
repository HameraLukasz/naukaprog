﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfApp3.ViewModel;

namespace WpfApp3
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        PeopleViewModel people = new PeopleViewModel();
        public MainWindow()
        {
            InitializeComponent();
            DataContext = people;
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            people.ReadData();
        }

        private void ButtonBase1_OnClick(object sender, RoutedEventArgs e)
        {
            people.WriteData();
        }
    }
}
