﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApp3.Model;
using Dapper;
using System.Data.SqlClient;
using System.Runtime.CompilerServices;
using PropertyChanged;
using WpfApp3.Annotations;

namespace WpfApp3.ViewModel
{
    class PeopleViewModel : INotifyPropertyChanged
    {
        public ObservableCollection<Person> People { get; set; } = new ObservableCollection<Person>();

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Age { get; set; }
        private string _test = "test";

        public string Test
        {
            get
            {
                return _test;
            }
            set
            {
                _test = value;
            }
        }

        public void ReadData()
        {
            People.Clear();
            using (IDbConnection db = new SqlConnection(@"Persist Security Info=False;Integrated Security=true;Initial Catalog=NaukaProgramowania;server=(local)"))
            {
                var query = @"SELECT * FROM People";

                var lit= Enumerable.Range(1, 20).ToList();

                db.Query<Person>(query).ToList().ForEach(x=> People.Add(x));

            }
            Test = "testtest";
        }
        public void WriteData()
        {
            using (IDbConnection db = new SqlConnection(@"Persist Security Info=False;Integrated Security=true;Initial Catalog=NaukaProgramowania;server=(local)"))
            {

                var query = $@"INSERT People (Id,First,Last,Age) VALUES ({People.Max(x=>x.Id)+1},'{FirstName}','{LastName}',{Age})";
                People.Add(new Person()
                {
                    Id = People.Max(x => x.Id) + 1,
                    Age = Convert.ToInt16(Age),
                    First = FirstName,
                    Last = LastName
                });
                db.Query(query);

            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
