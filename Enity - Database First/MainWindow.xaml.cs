﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Enity___Database_First;
using System.Reflection;
using Enity___Database_First;

namespace Enity___Database_First
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml

    public class ViewModel
    {
        public ObservableCollection<Order_Details> Orders { get; set; }
            = new ObservableCollection<Order_Details>( 
                new NorthwindEntities()
                .Order_Details
                .OfType<Order_Details>()
                .Where(x=>(x.Quantity*x.UnitPrice)>120)
                .ToList());
    }

    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new ViewModel();
        }
    }
}
